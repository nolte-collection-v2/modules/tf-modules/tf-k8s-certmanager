output "release" {
  value = helm_release.this
}
output "namespace" {
  value = module.namespace.this
}
