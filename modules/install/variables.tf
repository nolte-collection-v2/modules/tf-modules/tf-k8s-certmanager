variable "namespace" {
  default = "cert-manager"
}
variable "chart_version" {
  default = "v1.0.4"
}


variable "extra_values" {
  default = {}
}

variable "namespace_metadata" {
  type = object({
    supporters = string
    solution   = string
  })
  default = {
    supporters = "cluster-admins"
    solution   = "technical"
  }
}
