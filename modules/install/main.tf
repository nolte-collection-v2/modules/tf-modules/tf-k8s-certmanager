module "namespace" {
  source   = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-namespace.git//modules/namespace"
  name     = var.namespace
  metadata = var.namespace_metadata
}

locals {
  EXTRA_VALUES = {
    installCRDs = "true"
    #webhook = {
    #  hostNetwork = "true"
    #}
  }
}

resource "helm_release" "this" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = var.chart_version

  namespace = module.namespace.this.metadata[0].name
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}
