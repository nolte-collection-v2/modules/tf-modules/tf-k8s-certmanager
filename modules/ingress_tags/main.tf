variable "cluster_issuer_name" {

}

output "annotations" {
  value = {
    "cert-manager.io/cluster-issuer" = var.cluster_issuer_name
  }
}
