
resource "kubectl_manifest" "this" {
  yaml_body = <<YAML
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: ${var.issuer_name}
spec:
  selfSigned: {}
  YAML
}

output "name" {
  value = var.issuer_name
}
output "this" {
  value = kubectl_manifest.this
}

module "annotations" {
  source              = "../ingress_tags"
  cluster_issuer_name = var.issuer_name
}

output "ingress_annotations" {
  value = module.annotations.annotations
}
