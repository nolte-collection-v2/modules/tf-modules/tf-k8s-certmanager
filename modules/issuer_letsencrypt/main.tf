
locals {
  issuer = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = var.issuer_name
    }
    "spec" = {
      "acme" = {
        "email" = var.issuer_letsencrypt_email
        "privateKeySecretRef" = {
          "name" = lookup(var.issuer_letsencrypt_secret, var.issuer_letsencrypt),
        }
        "server"  = lookup(var.issuer_letsencrypt_server, var.issuer_letsencrypt),
        "solvers" = var.issuer_letsencrypt_solvers
      }
    }
  }
}


resource "kubectl_manifest" "this" {
  yaml_body = yamlencode(local.issuer)
}


output "this" {
  value = kubectl_manifest.this
}
output "ingress_annotations" {
  value = {
    "cert-manager.io/cluster-issuer" : var.issuer_name
  }
}

