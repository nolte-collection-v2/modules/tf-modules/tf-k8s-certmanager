variable "issuer_name" {
  default = "cluster-issuer"
}

variable "issuer_letsencrypt" {
  default = "staging"
}


variable "issuer_letsencrypt_secret" {
  type = map(string)

  default = {
    staging = "letsencrypt-staging"
    prod    = "letsencrypt-production"
  }
}
variable "issuer_letsencrypt_server" {
  type = map(string)

  default = {
    staging = "https://acme-staging-v02.api.letsencrypt.org/directory"
    prod    = "https://acme-v02.api.letsencrypt.org/directory"
  }
}
variable "issuer_letsencrypt_solvers" {
  type    = list(any)
  default = []
}
variable "issuer_letsencrypt_email" {
}
